/* How to use a Force sensitive resistor to fade an LED with Arduino
   More info: http://www.ardumotive.com/how-to-use-a-force-sensitive-resistor-en.html 
   Dev: Michalis Vasilakis // Date: 22/9/2015 // www.ardumotive.com  */
   

//Constants:
const int ledPin = 3;     //pin 3 has PWM funtion
const int sensorPin = A0; //pin A0 to read analog input

//Variables:
int value; //save analog value
int ofValue;

void setup(){
  pinMode(ledPin, OUTPUT);  //Set pin 3 as 'output' 
  Serial.begin(9600);       //Begin serial communication
}

void loop(){
  value = analogRead(sensorPin);       //Read and save analog value from potentiometer
  Serial.println(ofValue);               //Print value
  Serial.write(ofValue);

  value = map(value, 0, 1023, 0, 255); //Map value 0-1023 to 0-255 (PWM)
  ofValue = analogRead(sensorPin); 
  ofValue = map(ofValue,0, 1023, 0, 100);
  
  analogWrite(ledPin, value);          //Send PWM value to led
  delay(100);                          //Small delay
}
