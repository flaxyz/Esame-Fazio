#include "ofMain.h"
#include "ofApp.h"

int main( )
{
    ofGLFWWindowSettings settings;
    
    settings.decorated = true;
    //settings.setPosition(ofVec2f(0,0));
    settings.windowMode = OF_WINDOW;
    
    ofCreateWindow(settings);
    
	ofRunApp(new ofApp());

}
