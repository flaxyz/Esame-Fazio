#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup()
{
    serial.listDevices();
    serial.setup(0, 9600);
    
    ofSetFrameRate(60);
    ofSetVerticalSync(true);
    ofSetBackgroundColor(0);
    ofSetCircleResolution(360);
    
    idle = new Idle();
    idle->setup();
    
    detect = new Detection();
    detect->setup();
    
    mode = IDLE;
    
    
    ofSetFrameRate(60);
    ofBackground(0);
    mesh.setMode(OF_PRIMITIVE_POINTS);
    pressed = false;
    for (int i = 0; i < num; i++) {
        particles[i].position = ofVec3f(ofRandom(ofGetWidth()), ofRandom(ofGetHeight()), ofRandom(-ofGetWidth(), ofGetWidth()));
        particles[i].depth = ofGetWidth();
    }
}


//--------------------------------------------------------------
void ofApp::update()
{
    detect->update();
    
    forceNoise = ofNoise(ofGetElapsedTimef() * 1.2f);
    
    mesh.clear();
    
    for(int i = 0; i < num; i++)
    {
        if (pressed)
        {
            particles[i].addAttractionForce(mouseX, mouseY, 0, ofGetWidth() * 1.5, 1.0);
        }
        
        particles[i].update();
        particles[i].throughOffWalls();
        mesh.addVertex(ofVec3f(particles[i].position.x, particles[i].position.y, particles[i].position.z));
    }

    
    if(serial.available() && bReceiveFromArduino)
    {
        unsigned char bytesReturned[3];
        
        serial.readBytes(bytesReturned, 4);
        
        string serialData = (char*) bytesReturned;
        forceValue = ofToInt(serialData);
        
        //ci serve per pulire da evenutali sporcizie
        serial.flush();
    }
}


//--------------------------------------------------------------
void ofApp::draw()
{
    setState(mode);
}

void ofApp::setState(StateMode m)
{
    switch (m)
    {
        case IDLE:
            bReceiveFromArduino = false;
            idle->draw();
            break;
            
        case ENGANGEMENT:
            detect->draw();
            break;
            
        case VIDEO:
            break;
            
        case PLAY:
            bReceiveFromArduino = true;
            drawParticles();
            break;
            
        default:
            break;
    }
}

//--------------------------------------------------------------
void ofApp::drawParticles()
{
    ofSetColor(64, 255, 255, 192);
    ofEnableBlendMode(OF_BLENDMODE_ADD);
    static GLfloat distance[] = { 0.0, 0.0, 1.0 };
    glPointParameterfv(GL_POINT_DISTANCE_ATTENUATION, distance);
    glPointSize(500);
    mesh.draw();
    ofDisableBlendMode();
}



//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
  
}


//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
    switch (key)
    {
        case '1':
            mode = IDLE;
            break;
            
        case '2':
            mode = ENGANGEMENT;
            break;
            
        case '3':
            mode = VIDEO;
            break;
            
        case '4':
            mode = PLAY;
            break;
            
        default:
            break;
    }

}


//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    pressed = true;
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
    pressed = false;
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
