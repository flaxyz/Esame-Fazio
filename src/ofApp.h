#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxTemplateMatching.h"
#include "Idle.h"
#include "Detection.h"
#include "ofxXmlSettings.h"
#include "ParticleSystem.h"
#include "ofxSimpleSerial.h"

class ofApp : public ofBaseApp
{
	public:
    
    enum StateMode
    {
        IDLE,
        ENGANGEMENT,
        VIDEO,
        PLAY
    };
    
    
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    
    Idle *idle;
    Detection *detect;
    static const int num = 50000;
    ParticleSystem particles[num];
    
    float forceNoise;
    
    StateMode mode;
    
    void setState(StateMode m);
    void drawParticles();
    
    ofSerial    serial;
    
    int forceValue;
    bool bReceiveFromArduino;
    
    ofVboMesh mesh;
    bool pressed;
    
};
