#ifndef ParticleSystem_h
#define ParticleSystem_h

#include <stdio.h>

#endif

#pragma once
#include "ofMain.h"

class ParticleSystem {
    
public:
    ParticleSystem();
    void addForce(ofVec3f force);
    void addForce(float forceX, float forceY, float forceZ);
    void update();
    void draw();
    
    void bounceOffWalls();
    void throughOffWalls();
    void resetToRandomPos();
    
    void addRepulsionForce(float x, float y, float z, float radius, float scale);
    void addRepulsionForce(ParticleSystem &p, float radius, float scale);
    
    void addAttractionForce(float x, float y, float z, float radius, float scale);
    void addAttractionForce(ParticleSystem &p, float radius, float scale);
    
    ofVec3f position;
    ofVec3f velocity;
    ofVec3f acceleration;
    float friction;
    float radius;
    bool bFixed;
    float mass;
    float depth;
    float minx, miny, minz;
    float maxx, maxy, maxz;
};
