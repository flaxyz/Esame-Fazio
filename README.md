ARDUINO, FORCE SENSITIVE RESISTOR, OPENCV, PARTICLE SYSTEM

<--------*************INTRODUCTION************------->
This is a prototype for a software for an interactive installation.
The final setup will be a vertical surface which represent an access to the energetic soul of planet earth. Pressing the surface, each user operates a positive exchange of energy with the planet. But not all the users are the same: they will need in fact "a key" to be recognised and start the exchange. This represent the fact that you need to take action in some way to operate a positive change (and exchange) in an environmental sense. While some of us are putting their efforts to make our world better, some others are showing indifference, that's why the surface will ignore them as well. On the contrary, if you are recognised, the software start working.
You can interact with the surface pressing it, and a particle system will be showing.
To express this functions, the software is a state machine with 4 phases: IDLE, HIDDEN PHASE, DETECTION, PLAY.



<--------*************DETECTION & HIDDEN PHASE**************------->
In our prototype we re-created the possibility of being recognised through an openCV algorythm: the haar-cascade.
The Haar-cascade is an object detection algorithm which uses a machine learning system to store the data. The system is provided with several numbers of positive images (like faces of different persons at different backgrounds) and negative images (images that are not faces but can be anything else like chair, table, wall, etc.), and the feature selection is done along with the classifier training using Adaboost and Integral images.
In our prototype, we used a simplified version of this system, which learns specific objects in real time. The file xml we used is called Template Matching and we used this one: https://github.com/vanderlin/ofxTemplateMatching. We recognise the key objects in a whatsocalled HIDDEN PHASE.
In the hidden phase we set all the templates that the machine will recognise in the next phase: DETECTION.
In the detection phase, the software is ready to start: when a real object is recognised, the user, who is wearing or holding it, starts to play.



<------------*************PLAY***************----------------->
The exchange of energy happens through applying pressure. To create this interaction we use Arduino ONE combined with two squared force sensitive resistors we bought here: https://www.amazon.it/dp/B004R277AS/ref=pe_3310731_185740161_TE_item.
You can find a similar one also here: https://www.sparkfun.com/products/9375.
This resistors are very sensitive and very well documented. We use this tutorial to match them with our Arduino: http://www.ardumotive.com/how-to-use-a-force-sensitive-resistor-en.html.